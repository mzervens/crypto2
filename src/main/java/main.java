import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v1CertificateBuilder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.bouncycastle.util.io.pem.PemWriter;

import javax.crypto.Cipher;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Calendar;
import java.util.Date;

public class main {

    private static void generateCertificate(String dir, KeyPair keys) throws Exception {
        X500Name issuer = new X500Name("CN=MatissZervens");
        X500Name subject = new X500Name("CN=MatissZervens");
        Date validFrom = new Date(); // from now

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 1);
        Date validTill = cal.getTime();

        BigInteger serialNumber = BigInteger.valueOf(8);

        SubjectPublicKeyInfo subjectPublicKeyInfo = SubjectPublicKeyInfo.getInstance(keys.getPublic().getEncoded());

        X509v1CertificateBuilder certificateBuilder = new X509v1CertificateBuilder(issuer, serialNumber, validFrom, validTill, subject, subjectPublicKeyInfo);

        ContentSigner sigGen = new JcaContentSignerBuilder("SHA256WithRSAEncryption").build(keys.getPrivate());

        X509CertificateHolder certificateHolder = certificateBuilder.build(sigGen);


        PemWriter writer = new PemWriter(new FileWriter(dir + "matiss-zervens-x509-certificate.PEM"));
        writer.writeObject(new PemObject("CERTIFICATE", certificateHolder.toASN1Structure().getEncoded()));
        writer.close();

        writer = new PemWriter(new FileWriter(dir + "matiss-zervens-x509-certificate.key"));
        writer.writeObject(new PemObject("PRIVATE KEY", keys.getPrivate().getEncoded()));
        writer.close();

        writer = new PemWriter(new FileWriter(dir + "matiss-zervens-x509-certificate.public"));
        writer.writeObject(new PemObject("PUBLIC KEY", keys.getPublic().getEncoded()));
        writer.close();
    }

    private static boolean verifyCertificate(String certPath, PublicKey publicKey) throws Exception {
        boolean isValid = true;
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        InputStream certificateInputStream = new FileInputStream(certPath);
        Certificate certificate = certificateFactory.generateCertificate(certificateInputStream);

        try {
            certificate.verify(publicKey);
        } catch (InvalidKeyException e) {
            isValid = false;
        } catch (SignatureException e) {
            isValid = false;
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(certificate.getEncoded());
        X509Certificate x509 =  (X509Certificate) certificateFactory.generateCertificate(bais);

        // Get subject
        Principal principal = x509.getSubjectDN();
        String subjectDn = principal.getName();

        // Get issuer
        principal = x509.getIssuerDN();
        String issuerDn = principal.getName();

        if (!issuerDn.equals(subjectDn)) {
            isValid = false;
        }

        return isValid;
    }

    private static PublicKey readPublicKey(String pubKeyPath) throws Exception {
        PemReader publicReader = new PemReader(new FileReader(pubKeyPath));
        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(publicReader.readPemObject().getContent()));
        return  publicKey;
    }

    private static PrivateKey readPrivateKey(String privateKeyPath) throws Exception {
        PemReader privateReader = new PemReader(new FileReader(privateKeyPath));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateReader.readPemObject().getContent()));
        return privateKey;
    }

    private static byte[] encryptWithRSA(String data, PublicKey key) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(data.getBytes());
    }

    private static byte[] decryptWithRSA(byte[] data, PrivateKey key) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(data);
    }

    public static void main(String args[]) throws Exception{
        Security.addProvider(new BouncyCastleProvider());

        while (true) {
            System.out.println("Choose (1) Generate X509 certificate (2) Verify X509 certificate (3) Encryption (4) Decryption");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String input = reader.readLine().trim();

            if (input.equals("1")) {

                System.out.println("Input a path to the directory where the certificate files will be stored");
                String dir = reader.readLine().trim();

                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                KeyPair keyPair = keyPairGenerator.generateKeyPair();
                generateCertificate(dir, keyPair);
            } else if (input.equals("2")) {

                System.out.println("Input a path to certificate");
                String cert = reader.readLine().trim();

                System.out.println("Input a path to public key");
                String publicKey = reader.readLine().trim();

                PublicKey fromFile = readPublicKey(publicKey);

                boolean verificationStatus = verifyCertificate(cert, fromFile);

                if (verificationStatus) {
                    System.out.println("Verification was successful!");
                } else {
                    System.out.println("Verification failed!");
                }

            } else if (input.equals("3")) {
                System.out.println("Input a path to public key");
                String publicKeyPath = reader.readLine().trim();
                PublicKey publicKey = readPublicKey(publicKeyPath);

                System.out.println("Input a path to result file");
                String resultPath = reader.readLine().trim();

                System.out.println("Input a message to encrypt: ");
                String message = reader.readLine().trim();
                byte[] encrypted = encryptWithRSA(message, publicKey);

                OutputStream os = new FileOutputStream(resultPath);
                os.write(encrypted);
                os.close();

            } else {
                System.out.println("Input a path to private key");
                String privateKeyPath = reader.readLine().trim();
                PrivateKey privateKey = readPrivateKey(privateKeyPath);

                System.out.println("Input a path to encrypted data");
                String path = reader.readLine().trim();

                System.out.println("Input a path to result data");
                String resultPath = reader.readLine().trim();

                InputStream inputStream = new FileInputStream(path);
                long fileSize = new File(path).length();
                byte[] allBytes = new byte[(int) fileSize];
                inputStream.read(allBytes);


                byte[] decrypted = decryptWithRSA(allBytes, privateKey);
                String result = new String(decrypted);
                PrintStream out = new PrintStream(new FileOutputStream(resultPath));
                out.print(result);
                out.close();

            }

        }
    }

}
